provider "aws" {
 region   = "us-east-1"
}
resource "aws_instance" "mysql-automation" {
    ami                         = var.linux_ami
    instance_type         = var.linux_instance_type
    subnet_id                   = var.subnet_id
    vpc_security_group_ids     = [var.sg_id]
    key_name                    = "my-ec2-key"

    root_block_device {
        volume_size             = var.linux_root_volume_size
        volume_type             = var.linux_root_volume_type
        delete_on_termination   = true
        encrypted               = true
    }

    tags = {
      Name                      = "mysql-automation"
    }
    
}