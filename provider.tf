#AWS Provider
terraform {
    required_providers {
       aws = {
          source = "hashicorp/aws"
          version = "~> 3.0"
          #region   = "us-east-1"
       }
    }

    
       backend "s3" {
       bucket                      = "tksterraformstate"
       key                         = "terraform.tfstate"
       region                      = "us-east-1"
       encrypt                     = true
       dynamodb_table              = "terraform-state-lock"
       #skip_credentilas_validation = true
       }
#provider "aws" {
#  region   = "us-east-1"
#}
}