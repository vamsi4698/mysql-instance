variable "linux_ami" {
    type    = string
    description = "Ec2 instance type for linux server"
    default   = "ami-080e1f13689e07408"
}

variable "subnet_id" {
    type    = string
    description = "Ec2 instance type for linux server"
    default   = "subnet-026067b659a7940d8"
}

variable "vpc_id" {
    type    = string
    description = "Ec2 instance type for linux server"
    default   = "vpc-08cbc9b3287b96380"
}

variable "sg_id" {
    type    = string
    description = "Ec2 instance type for linux server"
    default   = "sg-04801315b6dd24c1e"
}

variable "linux_root_volume_size" {
    type    = string
    description = "Ec2 instance type for linux server"
    default   = "8"
}

variable "linux_root_volume_type" {
    type    = string
    description = "Ec2 instance type for linux server"
    default   = "gp2"
}

variable "linux_instance_name" {
    type    = string
    description = "Ec2 instance type for linux server"
    default   = "mysql-automation"
}

variable "linux_instance_type" {
    type    = string
    description = "Ec2 instance type for linux server"
    default   = "t2.micro"
}